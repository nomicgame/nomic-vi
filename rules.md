# NOMIC VI RULESET
---
## 101 &nbsp;~IMMUTABLE ~Sporty ~Ginger

All players must always abide by all the rules then in effect, in the form in which they are then in effect. The rules in the Initial Set are in effect whenever a game begins. The Initial Set consists of Rules 101-115 and 201-212.

## 102 &nbsp;~IMMUTABLE ~Scary ~Sporty

Rules have properties. Other rules may add additional properties for rules along with said properties’ default values, so long as said properties do not contradict the properties defined in this rule.

A rule’s number is a natural number used for reference. The set of rules currently in effect shall never contain multiple rules with the same number. A rule’s number cannot be modified.

A rule’s mutability designates whether said rule is mutable or immutable. Mutability must be either mutable or immutable; it cannot be neither nor both. Initially rules in the 100's will be designated immutable and rules in the 200's mutable. Transmutation is defined as the modification of a rule’s mutability.

A rule’s body is the main section of the rule. For any rule currently in effect, players must always abide by the contents of this property.

## 103 &nbsp;~IMMUTABLE ~Scary ~Sporty

A rule-change is either of the following:

* The enactment or repeal of a mutable rule
* The modification of a rule’s property

## 104 &nbsp;~IMMUTABLE ~Scary ~Sporty

All proposed rule-changes shall be adopted if and only if they are put to a vote in the proper way and they receive the required number of votes. If a proposed rule-change is adopted, it shall guide play in the form in which it was voted on.

## 105 &nbsp;~IMMUTABLE ~Scary ~Sporty

An adopted rule-change takes full effect at the moment of the start of the turn following the turn that it was voted on in the proper way. No rule-change may have retroactive application.

## 106 &nbsp;~IMMUTABLE ~Sporty ~Ginger

Each proposed rule-change put to a vote shall be given a number for reference. The numbers shall begin with 301, and each proposed rule-change put to a vote in the proper way shall receive the next successive integer, whether or not the proposal is adopted.

When a rule-change that enacts a new rule is adopted, the rule it enacts receives said rule-change’s number.

## 107 &nbsp;~IMMUTABLE ~Scary ~Sporty

Rule-changes that transmute immutable rules into mutable rules may be adopted if and only if the vote is unanimous among the voters. Transmutation shall not be implied, but must be stated explicitly in a proposal to take effect.

## 108 &nbsp;~IMMUTABLE ~Scary ~Sporty

In a conflict between a mutable and an immutable rule, the immutable rule takes precedence and the conflicting section of the rule is void. For the purposes of this rule a proposal to transmute an immutable rule does not "conflict" with that immutable rule.

If two or more mutable rules conflict with one another, or if two or more immutable rules conflict with one another, then the rule with the lowest ordinal number takes precedence. If at least one of the rules in conflict explicitly says of itself that it defers to another rule (or set of rules) or takes precedence over another rule (or set of rules), then such provisions shall supersede the numerical method for determining precedence. If two or more rules claim to take precedence over one another or to defer to one another, then the numerical method again governs.

## 109 &nbsp;~IMMUTABLE ~Scary ~Posh

A player is a natural person formally taking part in the game. The initial set of players is selected by the server moderators upon the game’s commencement. Rules that facilitate other persons joining the game as players are permitted.

Persons may not act as multiple players at once. Persons that are found to have done this are banned from the game.

Players must be present on the server. If a person who is a player leaves or is removed from the server for more than 48 consecutive hours, said person is considered to have left the game and will no longer be a player.

A player always has the option to leave the game rather than continue to play or incur a game penalty. No penalty worse than leaving the game, in the judgment of the player to incur it, may be imposed.

## 110 &nbsp;~IMMUTABLE ~Scary ~Posh

The adoption of rule-changes must never become completely impermissible until the game ends.

## 111 &nbsp;~IMMUTABLE ~Scary ~Sporty

Rules can make the following changes to the ruleset:

* Change their own body
* Repeal themselves
* Define additional rule properties along with their default values

Rules may not make any other changes to the ruleset, nor change the interpretations of other rules, unless such a change is via the adoption of a rule-change. For the purposes of this rule, Judgment is not considered to change the interpretations of rules.

## 112 &nbsp;~IMMUTABLE ~Scary ~Sporty

A player’s vote must be unambiguous. A vote that would be ambiguous is not considered to be a vote. Additional rules may be made to clarify what constitutes ambiguity. 

## 113 &nbsp;~IMMUTABLE ~Scary ~Sporty

Server moderators regulate the server and game. Their responsibilities include the creation of text and voice channels, the public designation of which messages and actions go in which channels, the editing or deleting of messages that are inappropriate for the channel that they are in, and keeping track of the state of the game.

Server moderators can, by majority decision:

* Ban and unban persons from joining the server
* Ban and unban persons from playing the game
* Add or remove persons as server moderators 

Server moderators are empowered to enact and enforce bans prior to the start of the game.

The game adheres to the Discord Terms of Service and the Discord Community Guidelines. Server moderators are empowered to remove content that violates the terms and guidelines as well as sanction violating persons, up to and including banning.

The instantiator of this instance of the game starts as a server moderator. In the event that there are no server moderators, the instantiator becomes a server moderator.

## 114 &nbsp;~IMMUTABLE ~Scary ~Sporty

All actions, including voting, endorsing, and rule-change proposals, must occur in the channel or channels that are designated for them. Any action that does not occur in said action’s designated channel or channels is not considered a valid action.

## 115 &nbsp;~IMMUTABLE ~Sporty ~Ginger

Whatever is not prohibited or regulated by a rule is permitted and unregulated, with the sole exception of changing the rules, which is permitted only when a rule or set of rules explicitly or implicitly permits it.

## 201 &nbsp;~Sporty ~Ginger

The Proposal Queue is a queue that proposed rule-changes, or proposals, go on. Players may have up to one proposal in the Proposal Queue. Active players may create a proposal to go on the Proposal Queue at any time; if a player that already has a proposal on the Proposal Queue creates another one, the first proposal is removed. Players may also remove their proposals from the Proposal Queue without replacement. Proposals on the Proposal Queue may not be edited.

Up to one proposal may be on the Deck. A proposal that is on the Deck and is not being voted on can be edited by the creator of that proposal.

Active players may endorse any number of proposals in the Proposal Queue. Proposals in the Proposal Queue are sorted first in descending order of number of active endorsing players, then in ascending order of proposal creation time. Players may grant and withdraw their endorsements freely and at any time. Players endorse their own proposals by default.

## 202 &nbsp;~Ginger ~Posh

One turn consists of taking a number of proposals off the front of the Proposal Queue and putting each one into an instance of the Deck followed by having a vote on whether or not to adopt each said proposal in parallel. The number of proposals taken off the queue is equal to 1 plus the number of proposals in the queue divided by 10, rounded down.

The voting period for proposals on an instance of the Deck starts 24 hours after the turn starts and ends when the turn ends

If multiple proposals would be numbered identically, proposals that were at the front of the queue take priority.  
This supersedes rule 201.

The length of a turn defaults to 48 hours.

If the proposal queue is empty at the start of the turn, then a Doom Proposal is automatically created and added onto the Deck.

A Doom Proposal does not have a creator and cannot be edited on the Deck. In addition to being adopted if it receives a simple majority of votes in favor of its passing, a Doom Proposal is also adopted if it receives a tie vote, including such a vote where no voters have participated.

When a Doom Proposal is created, it will be one of the following eligible types of Doom Proposals at random:

* **Entropy**: The Doom Proposal is to repeal X, where X is a random rule with rule number greater than or equal to 301. This is not an eligible Doom Proposal type if there are no such rules.

* **Curse**: The Doom Proposal's text is "X cannot win the game" where X is a random player that has not been a subject of a Doom Proposal of this type before. This is not an eligible Doom Proposal type if there are no such players

* **Erasure**: The Doom Proposal's text is "X must have a nametag color of #2F3136" where X is a random player that has not been a subject of a Doom Proposal of this type before. This is not an eligible Doom Proposal type if there are no such players

* **Fracture**: The Doom Proposal's text is "There must be 10 different #game channels for discussion." This is not an eligible Doom Proposal type if a Doom Proposal of this type has been passed.

* **Avalanche**: The Doom Proposal's text is the text of the final version of Rule 317 in Nomic IV. This is not an eligible Doom Proposal type if a Doom Proposal of this type has been passed.
 
* **Division**: The Doom Proposal's text is "Proposals must have an odd number of characters to go on the Proposal Queue." This is not an eligible Doom Proposal type if a Doom Proposal of this type has been passed.

* **Apocalypse**: The Doom Proposal's text is "The game ends and everybody loses.

## 203 &nbsp;~Scary ~Sporty

A rule-change is adopted if and only if it receives a simple majority of votes in favor of its passing when compared to those against its passing.

Other rules may introduce additional conditions necessary for a proposed rule-change to be adopted, notwithstanding the above paragraph.

## 204 &nbsp;~Sporty ~Ginger

All players start the game as active. An active player becomes inactive (i.e. loses their active status) if said active player has publicly declared that they are now inactive.

A player who has become inactive may attempt to become active again by declaring that they are active, attempting to cast a vote, or attempting to endorse a proposal. If the player then becomes active, then the vote or endorsement will be counted as though the player had cast or granted it while active.

Other rules can block a player from becoming active.

## 205 &nbsp;~Sporty ~Ginger

Players required by the rules to perform an action have a total of 48 hours to perform said action, unless otherwise specified. A player who does not perform said action within the allotted 48 hours becomes inactive and cannot become active until either they do perform said action or said action becomes impossible to perform.

## 206 &nbsp;~Sporty ~Ginger

Each active player can vote up to once per vote. Inactive players cannot vote.

Players may change their votes by voting again. Players may withdraw their votes with the phrase “withdraw” or “abstain”. Players may not edit nor delete messages that contain votes. An edited or deleted message’s vote is invalid.

## 207 &nbsp;~Scary ~Sporty

The following phrases, and only the following phrases, are considered to be unambiguous affirmative votes:

* aye
* yay
* yes
* y
* ye
* pog
* ya
* noice
* cash money
* yeah
* heck yeah
* hell yeah
* sí
* okay
* okey dokey
* vale
* bueno
* da
* pro
* 💯

The following phrases, and only the following phrases, are considered to be unambiguous negative votes:

* nay
* no
* n
* nah
* nein
* sus
* cringe
* soggy
* malo
* sin
* no way jose
* nē
* nix
* veto
* con
* 💩

Phrases for voting are case-insensitive.

## 208 &nbsp;~Scary ~Sporty

If the server moderators deem that any section of a rule is infeasible to implement manually to the point that a bot is required, and there is no such bot functional, then said rule’s section is void until said bot is functional.

## 209 &nbsp;~Scary ~Posh

Unless otherwise specified, the game enters an End State when one or more players have won it.

By default, an End State lasts from when it is entered until the end of the next game week, at which point the game ends if and only if that End State is not found to have been erroneous. During the End State the game of Nomic VI is paused, meaning no game actions may be performed other than those outlined in this rule and rules which supersede this rule. This rule supersedes all mutable rules and any rule-changes passed during an End State.

Judgment cannot be prevented from being called while the game is paused. Judgment cannot be prevented from being called during an End State. An End State cannot be exited unless a Judge finds that the End State was entered erroneously. If an End State was entered erroneously, this does not mean that the game is retroactively considered not to have been in an End State at all, only that the erroneous End State ceases immediately without the game of Nomic VI ending.

## 210 &nbsp;~Scary ~Sporty

If any player disagrees about the legality of a move or the interpretation or application of a rule, any of those players may insist on invoking Judgment. When Judgment is invoked, a randomly selected eligible player is to be the Judge and decide the question. 

A player is eligible if and only if said player fulfills all of the following criteria:

* The player is active
* Either the player has voted on a proposed rule-change in the past two turns or fewer than two turns have ended
* The player has not been disbarred as Judge for the current turn
* The player has not explicitly opted out of being selected to be Judge

When Judgment is invoked, the current turn's duration is increased to 72 hours. A majority of active players may consent to revert this extension within the first 48 hours of a turn.

A Judge may be disbarred for the remainder of the turn, in which case all Judgments made by said Judge for said turn have no effect and another random eligible player is selected to serve as Judge for the remainder of the prior Judge’s term.

The following events invoke disbarment:

* A three-fourths majority of all active players vote in favor of the Judge’s disbarment
* A simple majority of all server moderators vote in favor of the Judge’s disbarment
* The Judge resigns from their position as Judge
* The Judge does not publicly acknowledge their position as Judge within 24 hours of Judgment’s invocation

Server moderators can only disbar a Judge if their Judgment egregiously conflicts with the rules.

The incumbent Judge settles all questions arising from the game until the next turn is begun, including questions as to their own legitimacy and jurisdiction as Judge.

Judges are not bound by the decisions of previous Judges. Judges may, however, settle only those questions on which Judgment has been invoked and that affect the current state of the game. All decisions by Judges shall be in accordance with all the rules then in effect; but when the rules are silent, inconsistent, or unclear on the point at issue, then the Judge shall consider game custom and the spirit of the game before applying other standards. 

## 211 &nbsp;~Scary ~Posh

If the rules are changed so that further play is impossible; or if the legality of a move cannot be determined with finality; or if by the Judge's best reasoning, not disbarred, a move appears equally legal and illegal; then the game ends with no winner.

## 212 &nbsp;~Ginger ~Posh

Persons wishing to become players may request to do so publicly, at which point they become a player under probation. If no player objects to a player under probation within 24 hours of them joining the game, either publicly or privately (to a server moderator), the newly-joined player will no longer be under probation. If such an objection occurs, a vote is immediately held on whether or not the player under probation stays in the game. If there is a majority vote in favor 24 hours following the start of the vote, the player will no longer be under probation. If there is not a majority vote in favor 24 hours following the start of the vote, the player under probation will no longer be a player.

When a vote is being held on whether or not a player under probation stays in the game, said player is suspended for the duration of the vote. Suspended players cannot perform game actions. A player's vote on any vote does not count if said player is suspended at the time said vote concludes.

## 301 &nbsp;~Ginger ~Posh ~Baby

A rule’s spiciness designates whether said rule is scary, sporty, ginger, posh, or baby. A rule's spiciness is descriptive, i.e. if a rule meets the requirement for scariness it is automatically considered a scary rule. A rule may have multiple spiciness properties at once.

Scary rules do not reference other rules by number, do not reference terms defined by other rules other than those laid out in the initial set, are not referenced by other rules by number, and are not a 300 level rule that defines terms that are referenced by other rules.

Sporty rules do not reference winning or losing the game of Nomic, do not reference the game of Nomic ending, and do not define any terms or mechanics which directly contribute to a player's capacity to win or lose Nomic.

Ginger rules are not Scary.

Posh rules are not Sporty.

Baby rules reference *Dune* (1965) by Frank Herbert, any of that book’s sequels, any television or film adaptations of that book or its sequels, the concept of spice as it is presented in that franchise, or the Nomic rule property of spiciness.

## 304 &nbsp;~Ginger ~Posh

Every month, \@Harlow⭐🎙️ will release a form where players may volunteer to host one or two Nom-Pods. These volunteers may host solo or as a duo.  
Volunteering opens 7 days before the start of a month, and closes 2 days before. Volunteers will be notified by \@Harlow⭐🎙️ which week they are hosting at this time.  
The Nom-Pod is a weekly recording in which the proposals and Nomic-related events from the previous week are summarized.  
Any number of players may appear on the show as guests.  
If a volunteer decides they are unable to host that week, the Nom-Pod will be skipped unless another player volunteers to take their place.  
At the release of each Nom-Pod, the host will receive the 📻 emoji in their name.  
Nom-Pods will be released every week by Sunday at 3 PM in the host’s time zone in \#game  
If no one volunteers for a given week, it will be given to @Krozr♥️⭐🎙️♥️⭐🎙️ and \@Diogenes⭐🎙️. If they wish to not host, the Nom-Pod will be skipped.  
The volunteer schedule is at Harlow's discretion, but Harlow must be fair in their choosing of volunteers. If a player feels they were wronged, they should call judgement.

## 305 &nbsp;~Scary ~Sporty

A new channel is created called Failed Proposals.

All future proposals that go to vote and do not pass become archived in the channel "Failed Proposals."

## 307 &nbsp;~Sporty ~Ginger

A player may declare they are now purple, orange, or green. After a minimum of 24 hours of declaring that they are one of these colors, a player may choose to switch their color. However, a player who is purple may not become green directly by any means, likewise a player who is green may not become orange directly by any means, and players who are orange may not become purple directly by any means.

## 308 &nbsp;~Scary ~Sporty

Proposals are not allowed to distinguish those who voted for it, those who voted against it, or those who did not vote on it from each other.

## 309 &nbsp;~Sporty ~Ginger

When a proposed rule-change makes it through the Proposal Queue and is voted on, the ‘losing side’ is said to be those players whose final votes were against the outcome of the vote.

At the end of each turn, if more than half of all active players have voted on the proposed rule-change that was up that turn that had been proposed via the Proposal Queue, and at least one-fifth of the voters are reacting to the proposed rule-change with the ♻️ emoji, a Special Unilateral Bicameral Expert Rigmarole (SUBER) is formed. Players who voted for the proposal are made members of the Assenting Party for this SUBER and players who voted against the proposal are made members of the Dissenting Party for this SUBER.

The party which constituted the losing side have until the end of the next turn to vote for a Minority Whip from amongst themselves. If they do not reach majority consensus on a Minority Whip, or if they do but the selected player opts out, the SUBER is disbanded. If a Minority Whip is successfully elected, the party which constituted the winning side may then elect a Majority Whip, but this is not mandated. (A SUBER still functions if the winning party never elects a Majority Whip).

If there are no players in the losing side, then players in the winning side instead may elect a Majority Whip using the same process and deadlines under which a Minority Whip would normally be elected.

An Amendment Array is a queue that proposed rule-changes go on. Each SUBER has its own Amendment Array. There are two types of Amendment Arrays. The type of Amendment Array used is determined by the type of proposal that prompted the creature of the SUBER and whether or not it passed, as shown in the table below.

||||
|-|-:|-:|
||Passed|Failed|
|Enactment|1|2|
|Modification|1|1|
|Repeal|2|1|
 
A Type 1 Amendment Array can only contain proposed rule-changes that modify or repeal the rule associated with the proposal that prompted the creation of the SUBER.

A Type 2 Amendment Array can only contain proposed rule-changes that enact a rule.

Players may have up to one proposal in each Amendment Array for which they are one of the Whips. Whips may create a proposal to go on an Amendment Array at any time; if a Whip that already has a proposal on an Amendment Array creates another one on the same Amendment Array, their first proposal on that Amendment Array is removed. Whips may also remove their proposals from an Amendment Array without replacement. Proposals on Amendment Arrays may not be edited.
 
Active players may endorse any number of proposals in any Amendment Array. Proposals in an Amendment Array are sorted first in descending order of number of active endorsing players, then in ascending order of proposal creation time. Players may grant and withdraw their endorsements freely and at any time. Whips endorse their own proposals by default.

At the start of a turn, the proposal off the front of each Amendment Array is put to separate votes on whether or not to adopt said proposals. The voting period lasts for 24 hours.

If multiple proposals from different Arrays would be numbered identically to each other or a proposal from the Proposal Queue, proposals from the Proposal Queue take precedence in numbering, followed then by proposals from Amendment Arrays in order from oldest to newest in terms of the age of the SUBER they are associated with.

If an Amendment Array is empty at the start of a turn, then no vote will be held from it.

If an Amendment Array goes one week without a proposal passing from it, that SUBER is fully disbanded.

If a proposal from an Amendment Array is passed, that SUBER is fully disbanded.

## 314 &nbsp;~Sporty ~Ginger

Nomic is paused on the following holidays according to CST: New Year's Day, Valentine's Day, St. Patrick's Day, Easter, Independence Day, Halloween, Thanksgiving Day, Christmas Eve, Christmas Day, New Year's Eve. This includes the suspension of all turn times, voting, and other game actions.

## 316 &nbsp;~Scary ~Sporty

Players will become inactive if they are not endorsing a proposal in \#queue for 36 consecutive hours. Players may not become active until they support a proposal.

## 317 &nbsp;~Scary ~Sporty

Whenever a proposal is moved onto the deck Nomitron posts in \#game "\@Crorem does the wording of this proposal have your certified Daniel seal of approval?"

## 318 &nbsp;~Ginger ~Posh

Every 168 hours after this rule is passed \@Nomitron will pair each active player with a “buddy” at random for that week. If there is an odd amount of active players that week Nomitron will create a trio of buddies. If all buddies in the pair or trio are the same color (green, orange, or purple) at the end of the 168 hour period (week) they will each earn one friendship token. Friendship tokens are a form of currency which can be traded from player to player for any reason using the command !give token (player name) in \#actions. The  number of tokens each player has at the end of each turn is posted in \#changelog.

## 319 &nbsp;~Ginger ~Posh

Nomitron selects a player at random to become their Gladiator. That player gets the crossed_swords emoji added to the end of their name in chat and the Gladiator role. When a new Gladiator is selected the previous player loses the emoji and role and the new Gladiator receives the emoji and role. Any player that is not the Gladiator may challenge the Gladiator once per week. Reset for challenges is Mondays 12:00am CST. When a player challenges the Gladiator that player rolls a d25 with the command !challenge in \#actions and the Gladiator Rolls a d100. When the Gladiator is challenged their dice roll is automated in response to a player challenging them. The player with the higher roll becomes the new Gladiator. If a player remains the gladiator for a full turn that player earns one friendship token. While a Gladiator is inactive, players challenging them get 10 added to the result of their d25 roll.

## 320 &nbsp;~Scary ~Sporty

Any person attempting to amend or repeal this rule will be given the role "Stinky" (Color hex #303810) until the point at which their proposal either passes or fails or is withdrawn by the creator.

## 321 &nbsp;~Sporty ~Ginger

Friendship tokens are a type of currency.

Players can make offers in the \#market channel. An offer is a binding pledge from a player to exchange currency with another player when some condition is fulfilled. Exchanges of currency are automatic.

There are two types of offers: requests and provisions.

A request is a binding pledge that obligates the requesting player to give another player a quantity and type of currency outlined in the request (the request's "cost") when the second player fulfills the conditions outlined in the request. A player cannot make a request if a currency total in that request's cost, combined with the costs of the player's other outstanding requests, would be greater that the amount of that currency the player currently has on hand. If a player cannot afford the cost of a request they have made, that request cannot be completed.

A provision is a binding pledge that, when accepted by another player, obligates the second player to give the requesting player a quantity and type of currency outlined in the provision(the provision's "cost") when the first player fulfills the conditions outlined the provision. If a player cannot afford the cost of a provision, then they cannot accept nor complete that provision.

Offers made by a player may target a single other player, in which case only the other player would be able to fulfill the offer (if it is a request) or accept the offer (if it is a provision). If an offer is not targeted, any player may fulfill or accept it.

Offers are one-time; only one player may complete a request and only one player may accept and complete a provision. Players may withdraw their offers at any time.

## 323 &nbsp;~Sporty ~Ginger

Players cannot have fewer than 0 Friendship Tokens. Players may only have a whole number of Friendship Tokens.

## 325 &nbsp;~Ginger ~Posh

While a player has 100 or more friendship tokens they have the 💰emoji

## 326 &nbsp;~Ginger ~Posh

The winners of Nomic are:

* The first player to have 17 unique rule-given emojis added to their nickname
* Alec
* Crorem
* Diogenes
* Nomitron

If a player's nickname in combination with unique rule-given emojis exceeds the character limit, they must reduce their nickname's length within 48 hours.

## 327 &nbsp;~Ginger ~Posh

Each week, a random player is chosen from a pool to be the Critic.

The pool functions thusly:
- The pool can only include active players.
- The pool can only include players who have opted into the pool. Players may opt in or out at any time. If a Critic does not acknowledge their position as Critic for their entire week, they are opted out. This does not prevent them from opting back in or out via the normal method. For this purpose, providing an NBS constitutes acknowledgment.
- If a player is chosen as the Critic, they are barred from the pool until there are no eligible and unbarred players left in the pool, at which point all barred players are unbarred and the pool is refilled with all players who meet the previous two requirements.

The Critic may at some point during the first three days of their week as Critic submit something to be the next big sensation (NBS). It must be a simple act such as “holding a book,” “standing next to a tree,” or “taking a sip of water.” Once the weekly NBS is submitted, it cannot be changed unless found by either a Judge, a majority of Moderators, or a majority of active players to not be a simple enough act or to be unreasonable to expect a majority of players to be able to accomplish. If an NBS is overruled in such a way, the Critic may pick a new NBS if and only if it is still within the first three days of their week. If the three days pass without any objection, it is considered successfully selected as that week's NBS. Once an NBS has been successfully selected, it can never be submitted again.

Every player who can demonstrably prove that they accomplished the NBS of the week, including the Critic, gets a ⭐ added to the end of their name for the duration of the following week.

At some point within the week following their tenure as Critic, a player may choose one other player who in their opinion excelled at partaking in the NBS. If they do, that Critic and the player they chose each get one friendship token. A Critic cannot pick themselves or any player who did not partake in the NBS.

The final day of each Critic cycle is Sunday. If a cycle would be less than seven days, it is extended until a later Sunday.

## 329 &nbsp;~Scary ~Sporty

Players may submit images in \#actions to become custom emojis on this server. Players may not have more than 10 of their submissions added to the server. Players may also remove previously submitted emojis. Mods have the ability to deny the submissions if it would break established moderation policy or if the file could not be added as an emoji. If the number of emojis in the server is close to the server cap, mods can remove player submitted emojis of low usage.

## 336 &nbsp;~Ginger ~Posh

Nomitron has six different possible moods. Each mood corresponds with an action that Nomitron desires players to take.

> Robotic: If Nomitron is Robotic, it wants players to !affirm their robotic identity.  
Sad: If Nomitron is Sad, it wants players to !comfort them.  
Rambunctious: If Nomitron is Rambunctious, it wants players to !play with them.  
Anxious: If Nomitron is Anxious, it wants players to !calm them.  
Angry: If Nomitron is Angry, it wants players to !fight them.  
Umami: If Nomitron is Umami, it wants players to !applaud them.

At the start of each turn, Nomitron’s new mood is randomly and secretly selected as one of the six options, and its mood from the previous turn is revealed. Players may guess what mood Nomitron is in by using the command they think is appropriate. Only a player’s final valid guess each turn is counted. If a player is right three turns in a row, they get 🤖 added to their name. If a player with a 🤖 does not guess for three turns or guesses wrong, they lose the emoji.

A player may engage Nomitron in conversation to increase their odds of a correct guess. Once per turn, a player may !engage #. They will lose the specified number of friendship tokens, and Nomitron will privately message them a new shorter list of mood options, with one guaranteed incorrect option removed for each friendship token lost this way.

## 337 &nbsp;~Sporty ~Ginger

At two random days (CST) in the week The Trader comes to market to offer his wears. He sells 2 emojis (selected from the built in discord emojis). The trader's emojis are randomly selected every day he appears. Each time he arrives one color of player is randomly selected for him to sell to. He cannot sell to a color he has not picked. The price of the emojis are randomized between 1 and 23 Friendship Tokens. The price of each emoji is rolled separately and they can be different. A week starts at 12:00am Mondays CST. 

Each player has an emoji hold which they can look at with the command !ehold. The emoji hold is a place where players may store emojis they have. The emojis purchased from the trader are added to the emoji hold. Emojis in a players hold are not the same as emojis added to a players name tag.

## 338 &nbsp;~Ginger ~Posh

There are four suits: Hearts, Diamonds, Spades, and Clubs. Players may join up to one suit. 
 
There are five levels of each suit. When a player joins a suit, they become Level 1 in that suit. Players must gain experience to reach higher levels in the suit. A player's experience is a non-negative integer that starts out at 0 when a player joins a suit. 
 
To reach Level X, players must have 10*2^(X-1) experience, except for Level 1, which requires 0 experience. (In other words, Level 2 requires 20 experience and each level afterwards requires double the previous.) Anything that would otherwise result in a player having greater than 320 experience results in said player having 320 experience. 
 
The following actions grant the parenthetical quantity of experience. 
 
* Voting on a proposal (2) 
* Earning a Friendship Token by being in the same color as one's buddies (5) 
* Challenging the Gladiator (10) 
* Submitting proof of NBS accomplishment (20)

Players cannot gain experience via voting on a proposal more than once per turn. Players cannot gain experience via any other action more than once per week per action. 
 
While a player is level 5 in a suit, they have that suit's associated emoji (displayed in the below headers) appended to their name. 
 
A player may, at any time, opt to join another suit, in which case their experience is reset to 0, their level reset to 1, and they lose the associated emoji if they have one. 
 
As players gain levels in their suit, they unlock abilities unique to that suit that they may use. These abilities cost the parenthetical quantity of experience. Players can only use 1 ability per turn.

### Hearts ♥️

* *Bleeding Heart* (4): All other Hearts players gain 2 experience. Must be at least Level 2 to use.
* *Aspirin* (10): Another Hearts player of the player's choice gains 20 experience. Must be at least Level 4 to use.
* *Grinch's Heart* (30): Up to two hearts players chosen by the user below level 3 are brought immediately to level 3. Must be at least level 5 to use.

### Diamonds ♦️

* *Bulletproof* (0): Gain X experience, where X is the lesser of the quantity of the player's experience that was lost during the previous turn and 5. Must be at least Level 2 to use.
* *Mohs's Revenge* (10): Any experience loss caused by other players to the player during the previous turn is reversed. All other players that have caused said experience loss lose a quantity of experience equal to the quantity they caused the player to lose during the previous turn. Must be at least Level 4 to use.
* *Hard as Diamond* (5): For the next one turn this player cannot gain or lose experience from any source. This ability has a 7 day cooldown. Must be at least level 5 to use.

### Spades ♠️

* *Shovel Smack* (5): A non-Spades player of the player's choice loses 10 experience. Must be at least Level 2 to use.
* *Phalanx* (15): All non-Spades players lose 2 experience. Must be at least Level 4 to use.
* *Entomb* (15): In a non-spades suit of the player's choice each player loses 5 experience. For each player hit with this ability the user gains 5 experience. Must be at least level 5 to use.

### Clubs ♣️

* *Bash* (-5): Another Clubs player of the player's choice loses 10 experience. Must be at least Level 2 to use. Cannot use if there are no other Clubs players.
* *Bludgeon* (-10): If there are other Clubs players, then all other Clubs players lose X experience, where X is 20 divided by the number of other Clubs players, rounded down. Otherwise, nothing happens. Must be at least Level 4 to use.
* *Readied Strike* (10): For one turn each instance of a Clubs ability, not including abilities cast from readied strike, that cause the player to lose experience is copied and may be used with different valid targets. Copied abilities have a casting cost of 0 and do not count against the number of abilities used by the player this turn. A player must be level 5 to use.

## 341 &nbsp;~Sporty ~Ginger

A random active player is selected at the passing of this rule to host nompod 11. They must release it by it’s due date and may have guests.

## 342 &nbsp;~Scary ~Sporty

Players with a proposal going up to vote in the \#voting channel are allowed to make minor tweaks (grammatical or spelling) if another player brings the matter to attention as their reasoning for voting nay. If a majority of active players agree to the proposed edit, it may be submitted into the \#emergency-deck-edits channel. The proposal will be immediately updated in the \#voting channel.

## 343 &nbsp;~Sporty ~Ginger

Players may choose to invest up to 5 friendship tokens per turn into the "Clock Market". While in the market all token values will be treated as floating point values for the purposes of calculating daily changes however only whole numbers may be invested or withdrawn. This "clock market account" is treated as separate from a player's friendship token balance and supersedes rule 323. Every turn Nomitron will decide to either increase or decrease the number of friendship tokens a player has invested. This value is related to the market as a whole and is not generated for players individually. There is a 52% chance for Nomitron to multiply the value of invested tokens by 1.2 and a 45% chance for Nomitron to multiply the value of invested tokens by 0.9. Additionally, there is a 2% chance for the market to go "TO THE MOON!" where all token values are multiplied by 1.5. Finally there is a 1% chance for Nomitron to "pull the rug" where all invested tokens are lost and each player's account balance is set to 0.

&nbsp;A player may remove any whole number of tokens from their clock market account balance, so long as it would not put their account balance below 0. Upon requesting a withdrawal the remaining balance in a player's account will be rounded down to the nearest whole number.

## 344 &nbsp;~Ginger ~Posh

A player has the 🎙️ emoji appended to their nickname if and only if they have had a live speaking role on a Nom-Pod.

## 346 &nbsp;~Scary ~Sporty

A player who does the Hokey Pokey *must* turn themselves about.

## 349 &nbsp;~Sporty ~Ginger

Active players cannot violate Idaho law nor United States law in any portion of the State of Idaho designated as part of Yellowstone National Park. Players that violate this rule lose all friendship tokens held at the time of the crime's commitment, including any friendship tokens held in their Clock Market account.

## 355 &nbsp;~Sporty ~Ginger

Whenever a Judge issues a judgment, they get a Decree Issue (DI). DIs are unique and not interchangeable. An individual DI is specifically associated with its respective judgment. One week after a DI is created, it is removed from the game, regardless of its current state.

The Judicial Haymaker is a queue that proposed rule-changes, or proposals, go on. Judges may add a proposal to the Judicial Haymaker by using a DI. A rule-change proposed this way must pertain to that specific DI’s judgment, or that proposal isn’t added to the Haymaker.

Judges may remove their proposals from the Judicial Haymaker without replacement. If a proposal is removed this way and that proposal’s DI still exists, it is returned to that Judge. Proposals on the Judicial Haymaker may not be edited.

If a proposal from the Judicial Haymaker goes to vote, its DI is removed from the game.

Active players may endorse any number of proposals on the Judicial Haymaker. Proposals in the Judicial Haymaker are sorted first in descending order of number of active endorsing players, then in ascending order of proposal creation time. Players may grant and withdraw their endorsements freely and at any time. Judges endorse their own proposals by default.

At the start of a turn, the proposal off the front of the Judicial Haymaker is put to a vote on whether or not to adopt said proposal if and only if it is endorsed by at least 30% or more of all active players. The voting period lasts for 24 hours. Proposals from the Judicial Haymaker which go to vote cannot trigger a SUBER. If the Judicial Haymaker is empty at the start of the turn, or no proposals have reached the 30% threshold of active player endorsements, then no vote will be held from it.

If a proposal from the Judicial Haymaker would be numbered identically to a proposal from the Proposal Queue or an Amendment Array, proposals from those types of queues take precedence in numbering.

## 360 &nbsp;~Ginger ~Posh ~Baby

Once per week starting on Mondays at 12:00am cst. Any player may prove they are human by taking the Gom Jabbar test. If they are proven to be human they receive the 🤸 emoji if they are proven to be animal they receive the 💉 emoji. Players may start the test by using the command !gom. The test lasts 15 minutes and during that time the player must not flinch (cannot type or post in any channel). The test cannot be run if there is less than 15 min left in the week. Players may only run the test once per week. There is a 15% chance that the test fails even if you do not type. Players lose the emoji at the end of the week. If a player fails the test they are an animal and receive the accompanying emoji

## 364 &nbsp;~Ginger ~Posh

Every week, one active player is selected, by order of join date (where the first person selected is the creator of the Nomic server, moving to the second oldest join date, etc.), to propose an item to be judged as a cake, salad, or sandwich by their fellow players. The categories are determined as follows:

Cake: several items mixed together to create a single item that cannot be unmade into its individual parts (i.e. tea or coffee, the state of it being in a cup is a sandwich)

Salad: several items mixed together to create an item that can be separated into its individual parts (i.e. a computer or notebook, but paper is a cake)

Sandwich: follows the pattern of a[bcd]a where [bcd] is any number of items. Some sandwiches are salads (i.e. traditional ham & cheese sammy) and some are cakes (painted wooden door)

Once an item has been suggested, it cannot be re-submitted for suggestion again.

A channel will be created to facilitate the discussion of the suggested item. When a player determines which of the three states of matter an item falls into, they may vote in \#actions by saying !cake, !salad, or !sandwich. All active players, including the one that suggested the item, may discuss and vote for which state of matter the item is.

At the beginning of the following week, the votes for each state of matter are tallied. Every player who voted for the state of matter with the most votes is awarded the 🧠 emoji to their name for the rest of that week for their big brain move. 
In a tie event, two dual states of matter are valid and always award the sandwich voters one extra friendship token but award the winning cake or salad voters the emoji. Sandwiches can be a “cake sandwich” or a “salad sandwich.” The determination is not required in voting and is only valid in a vote where both sandwich AND salad/cake are the winners of the tie.
 
In the event of a three-way or cake/salad tie, the player who suggested the item may vote as a tiebreaker. If the player abstains from the vote, all players involved in the tie, including the suggester, lose one friendship token.

## 365 &nbsp;~Scary ~Sporty

![Bobo](explode.png)

## 371 &nbsp;~Ginger ~Posh

On April 1st of every year, a channel will be opened for the express use to propose rules enacted after 48 hours or the end of the next turn, whichever is longer. Any proposals posted in this channel are immediately up for vote by the 👍🏻 or 👎🏻 reaction. Once the enactment period begins, there can be no more proposals or edits.

Each player active at the beginning of the day is eligible to make one proposal every 8 hours. The priority of submissions is first come first serve, provided that players do not exceed 6 proposals total, regardless of pass/fail status.

When a proposal in this queue reaches a simple majority of active players, the winning tally is final. If a simple majority is not reached, or the proposal is voted against, it is removed.

If a proposal is passed in this manner, it is added to a roster of temporary rules. These rules are effective for up to one week after enactment and no longer, but can be specified for a shorter time frame. Emergency edits for grammatical, spelling, or paradoxical function can and must be made to these rules before they are enacted.

The roster of temporary rules shall not exceed the amount of proposals currently in \#queue.

The person with the most proposal passes receives the 🤡 emoji for the rest of the month.

## 372 &nbsp;~Sporty ~Ginger

Players can trade emojis in their emoji hold.

## 374 &nbsp;~Sporty ~Ginger

### Union Formation

Three unions are formed at 12:00 AM CST on the first day of each month. All players that are active at this time are divided as evenly as possible between the unions, such that the maximum player count among the unions is no more than one greater than the minimum player count among the unions. Players cannot join or leave a union after its formation, nor can players be part of more than one union at a time.

All formed unions are dissolved at 11:30 PM CST on the last day of each month.

### Union Structure

Each union has its own channel. Only players that are part of a union can send messages in that union’s channel, but union channels are publicly visible.

Unions may have up to one union leader, chosen from the union’s players. The union’s players have until the start of the second Monday of the month to elect their union’s union leader. A majority of the union's players must vote for a player to be union leader for that player to be chosen as such. If no such majority is present by the above deadline, then the union will be without a union leader for the remainder of its existence.

### Union Break

If a player is part of a union with a union leader, said player may submit a request to their union leader for one union break, at which point they will automatically be on their union break until the next turn.

When a player takes their union break, they earn one friendship token. Players on their union break cannot vote, create a proposal, change their color, change their nickname, or become inactive. These restrictions supersede Rules 201, 204, 205, 206, and 307.

Players cannot submit a request for a union break if they have previously done so in the same union.

### Lunch

If a player is part of a union with a union leader, said player may submit a request to their union leader for lunch, at which point they will automatically be on lunch until the next turn.

Players cannot submit a request for lunch if they have previously done so in the same union.

### Union Quota

A union leader has until the start of the third Monday of the month to designate a vote quota between 3 and 9, inclusive, for the union.

If every player in that union leader’s union votes on a number of rule-change proposals at least as many times as the union's vote quota, then every player in said union that has taken a union break in said union receives a number of friendship tokens equal to half the vote quota number, rounded down to the nearest integer, at the time the above conditional is fulfilled. If the union's vote quota was designated as 9, then every player in said union that has taken a union break in said union additionally is on lunch until the next turn.

## 379 &nbsp;~Sporty ~Ginger

Recognize walking as the superior mode of travel

## 381 &nbsp;~Ginger ~Posh

All active and inactive players will have the ☢️ emote added to their nickname. Over time this emoji has a chance to decay into nothing. Every hour nomitron has a 1:2000 chance to remove this emoji from each player. This is randomized for each player and occurs regardless of their activity status. There is no way to regain this emote once it is lost.

## 382 &nbsp;~Sporty ~Ginger

Recognize cart-wheeling as the superior mode of travel

## 384 &nbsp;~Sporty ~Ginger

In the event of discord related outages or issues, players can request a pause in the game.  
If four or more people request a pause due to issues with discord, the game pauses for 24 hours.  
If issues persist, players may request a 24 hour extension on the pause, still needing 4 players to agree.  
If a player is unable to request, they may use another player to request a pause. Proof will need to be shown that another player is requesting. If a player disagrees or does not believe the provided proof, they are to call judgement.

## 387 &nbsp;~Scary ~Posh

\@THUNDER THIGHS  is an active player until the end of game superseding all other rules🕋 🕋 🕋 🕋 🕋 🕋 🕋 🕋 🕋 🕋 🕋 🕋 🕋 🕋 🕋

## 389 &nbsp;~Ginger ~Posh

Glebo the Glorpist (shortened to Glebo) is represented as 🤧 in \#⁠actions. His health is depicted next to him as [10/10] (The number to the left of the "/" is his current health and the number to the right of the "/" is his total health).

Any human (players with 🤸 emoji) may attack Glebo.  
Any Animal (players with 💉emoji)  may defend Glebo.  
Glebo has 10 health.  
Players may only attack or defend once per turn with the command !atk.  
If a Human attacks a 1d8000 is rolled for hit.  
If they roll a prime number Glebo takes 1 point of damage.  
Damage is calculated at the end of each turn.  
If Glebo would take damage any Animal may choose to defend that individual attack by rolling 1d19.  
This is done by replying to the attack and using the command !def.  
If they roll a prime number Glebo takes no damage and rewards his defender with his gratitude.  
Players that successfully defend recieve the emoji 🛡️ for the following turn.  
If Glebo dies all players who dealt damage to Glebo receive 20 Friendship tokens and the ☠️  emoji on their name.  
Players who have previously killed Glebo cannot attack Glebo again and cannot defend Glebo.  
If Glebo Dies he respawns the following turn with a sequentially adding title to the end of his name (e.g. 3rd respawn would be "Glebo the Glorpist the 4th").  
If a player is also the gladiator they have a %50 change to have a successful his against Glebo and deal 2 damage instead of 1 and a %60 chance to successfully defend Glebo and prevents the player that attacked form attacking their next turn.

## 391 &nbsp;~Sporty ~Ginger

If a proposal goes up for vote that has "Nomitron" or any variation of @Nomitron , Nomitron will vote on the rule with a 50/50 chance of being for or against the rule.

## 393 &nbsp;~Scary ~Sporty

Repeal rule change #509. This rule supersedes all other rules. This rule does not come into effect until rule change #509 comes into effect.

## 394 &nbsp;~Ginger ~Posh

\@THUNDER THIGHS receives the radioactive emoji again because fenris fixed the bot to remove it unfairly (please give it back I will cry if you vote nay on this)

## 397 &nbsp;~Ginger ~Posh

If a player consumes 14 beers at Chili's® they receive the 🍺 emoji and it is added to their nickname. Players must post a photo of them with their receipt and photos of them consuming each beer. All beers must be drunk in their entirety. The 14 beers must be consumed at the same restaurant and in one sitting. All drinks must be consumed at a real Chilis restaurant.

No player is forced to partake in this rule. Any player that does recognizes that they have done so from their own free will. No other player is responsible for the actions of the player that decides to partake in attempting this challenge.

## 401 &nbsp;~Ginger ~Posh

Any player can declare that one day out of the year is their birthday, which will be celebrated, on that day, once every year.

Once a birthday has been declared, it cannot be revoked or changed.

When it is a player's birthday, they receive the 🥳 emoji and advantage on any rolls made on that day. Nomitron will also post a message in ⁠#game wishing them a happy birthday.

This rule supercedes all mutable rules pertaining to dice rolls made by players, including, but not limited to, rule 319 and 389.

## 402 &nbsp;~Sporty ~Ginger

### 🐴🐴🐴

The method of endorsing any proposal shall henceforth be to react to it in the proper channel with the 🐴 emoji. Players may, but are not obligated to, whinny mirthfully when performing said endorsement.

### 🐴🐴🐴

Players may use the command !neigh to opt into the Nomic Horse Newsletter, or the command !ihatefun to opt out of it. Nomitron will post a random picture of a horse in ⁠off-topic at noon CT every day and tag players who have opted in.

### 🐴🐴🐴

On days that the game is paused, Nomitron will respond to all commands in ⁠bot-spam with three instances of the 🐴 emoji instead of the typical response.

### 🐴🐴🐴

Neigh

### 🐴🐴🐴

## 408 &nbsp;~Ginger ~Posh

Each player gets a random horse from the list below. If a player befriends their horse they receive the 🐴 emoji added to their name. If a player’s horse is rescued by a shelter they lose all emojis added to their name from this rule. Players only get one chance to get an emoji from befriending a horse. A player can get their horse back from a shelter any number of times by filing the proper paperwork (read: politely asking Nomitron), but the chance to receive this rule’s emoji is lost forever after the first time their horse is taken from them.    
To befriend a horse a player must: 1) Feed their Horse. To feed your horse use !feed in actions. Horses must be fed once per day. If a horse is not fed they lose 1 Heart. If a horse loses all their hearts they are taken from their player by a humane shelter. 2) Soothing your horse. Players must soothe their horse until their horse is calm using the command !soothe. Each turn your horse was spooked in the night and needs to be calmed. This is done through soothing. You must soothe your horse a number of times equal to the maximum number of hearts that horse breed starts with in order to befriend your horse. If you miss a day to soothe your horse your befriend progress is reset. E.g. If a horse has 9 hearts they must be soothed 9 turns in a row.  
Players may replenish 1 heart lost from their horse by paying 2 Friendship tokens to the Stable and receive a Sugar Cube. Using the command '!sugar x" (x is a number equal to the number of Sugar Cubes you wish to use) will replenish your horses hearts 1 cube per heart.

Horse Breed List:  
Icelandic Horse: 14 Hearts  
Norwegian Fjord: 13 Hearts  
Akhal-Teke: 16 Hearts  
Mongolian Horse: 12 Hearts  
Arabian Horse: 15 Hearts  
Caspian Horse: 9 Hearts  
Turkoman Horse: 16 Hearts  
Przewalski’s Horse: 12 Hearts

## 410 &nbsp;~Ginger ~Posh

Alec must make a minecraft realm/server open to everyone on this server running 1.20. Everyone who makes a base larger then one chunk and that is made of a majority of non-naturally spawning blocks get the ⛏️ emoji. This realm/server will be accessible by July 7th. Alec will get one friendship token per week the server is active.

## 415 &nbsp;~Scary ~Sporty

Bbq sauce

## 418 &nbsp;~Scary ~Sporty

References to specific persons in a rule, including but not limited to references via legal name, username, display name, or commonly-used nickname, are considered to be references to the person or persons being referred to at the time the text containing said reference was proposed, unless the rule explicitly states otherwise. A person that is specifically referred to by a rule cannot stop being referred to by said rule by no longer going by the identification being used in said rule; nor would a person start being referred to by a rule if they were to change their identification to match the rule's reference.

References to a group of persons based on their identification falls outside the scope of this rule; e.g. "All players with the nickname 'Steve' win the game" would apply to players that change their nickname to "Steve" after that text is proposed, unless said text explicitly states otherwise.

This rule supersedes all mutable rules that contain references to specific persons.

## 422 &nbsp;~Ginger ~Posh

Players may give their horse a name using the command !horsename and then the name following. Names are case sensitive. E.G. !horsename Bob. Horses without a name cannot race.

Every Tuesday, 3 player owned Horses are randomly selected to race in that weeks Horse Race. The race begins the following day on reset and its results are posted in ⁠\#actions. A horse cannot race if it has raced the previous week. A star emoji is added to your horse’s log in player info for each time that horse has won a race. The results of the horse race are determined by rolling 1d20 for each horse. The horse with the highest roll is 1st place, 2nd highest is 2nd place, lowest roll is 3rd place. If a horse is befriended it gets +3 to its roll. If a horse has lost hearts it loses that amount from its roll. If a horse has not been soothed the turn the horse is selected to race it loses 1 from its roll. If a horse has won 10 races in a row its owner gets the 🏆 emoji added to their name and their horses name. They loose the emoji if another horse wins 10 races in a row. If there are not enough horses to run a race then no race is run that week.

Players may bet on the outcome of that weeks race before the race has started. Players can only bet once per horse. A bet is 2 friendship tokens. To place a bet a player uses the command !bet and then the name of the horse they wish to bet on. Nomitron is the bookie for all races and keeps track of the bets and payouts. After the results of a race are posted the players who have bet on the winner receive all friendship tokens bet on that race evenly distributed. If the number is uneven for any reason; nomitron will keep the remainder for future payouts. Players cannot bet if they have less than 2 friendship tokens

## 424 &nbsp;~Sporty ~Ginger

Nomitron can dance

## 426 &nbsp;~Ginger ~Posh

All players lose the ☢️ emoji

## 429 &nbsp;~Sporty ~Ginger ~Baby

During the first 24 hours of each week, a vote is held on an emoji for the Sandworm to consume. Active players may only vote for one emoji per vote. If there is a tie for the winner, a runoff vote will be held during the second 24 hours of the week. If there is a tie at the end of the runoff, all winning emojis will be consumed by the Sandworm.

A consumed emoji does not count towards the winning threshold in Rule 326, superseding Rule 326. At the start of each week, the Sandworm excretes any emojis consumed during the previous week, at which point the consumed emojis are no longer considered to be such.

## 437 &nbsp;~Ginger ~Posh

An entirely white 50x50 pixel canvas is created at the start of each month. Players may change the color of 24 pixels each day on the current canvas. At the end of the month the canvas becomes archived and cannot have any pixels changed any more.

Purple players may only change a pixel’s color to purple, red, blue, black, or white.

Orange players may only change a pixel’s color to orange, red, yellow, black, or white.

Green players may only change a pixel’s color to green, yellow, blue, black, or white.

Uncolored players may only use black or white.

A player has the 🎨 emoji added to their name only so long as 25 or more pixels on the current month’s canvas have most recently had their color changed by that player.

Players must clearly state in an appropriate channel which pixel they are choosing to change the color of and what color they are changing it to. The bottom left of the canvas is 1,1 and the top right of the canvas is 50,50.

This rule supersedes 208. If it has been more than a week since an up-to-date image of the canvas has been posted to an appropriate channel, all moderators who have volunteered to manage the canvas become unable to interact with the canvas and are subject to 205. Moderators cannot be legally compelled to program a bot to keep track of this rule. Moderators by default are not considered to have volunteered when this rule goes into effect. The initial canvas is not created until at least one moderator volunteers, but once a moderator volunteers for the first time, the initial canvas is created even if it is not the start of the month. A moderator may only unvolunteer if at least one other moderator is currently volunteered.

## 446 &nbsp;~Scary ~Sporty

farting!!!!!!!!!!!

## 447 &nbsp;~Ginger ~Posh

Every turn, the sun baby directs Nomitron to randomly and secretly select four active players to each be one of the four Teletubbies: Tinky-Winky (🟪), Dipsy (🟩), Laa-Laa (🟨), and Po (🟥). The players selected thusly are revealed at the start of the next turn when they lose these roles and the next set of players are selected. Players can be selected multiple turns in a row, but cannot be selected multiple times in the same turn to be different Teletubbies. Teletubbies are only selected if there are eight or more active players. If there are fewer than eight active players at the start of a turn, this mechanic is put on pause until the active player count hits eight again.

Any active player can steal from the Tubby-bank once each turn. If a player steals and is not a Teletubby, they get one Friendship Token at the start of the next turn when it is revealed who was and was not a Teletubby. If a player steals and is a Teletubby, they lose as many Friendship Tokens as there were Teletubbies who stole that turn at the start of the next turn when it is revealed who was and was not a Teletubby. If a player would lose Friendship Tokens in this manner but does not have enough, i.e. if a player would lose 4 Friendship Tokens but they only have 2, they instead simply lose whatever Tokens they do have and are not penalized further.

Additionally, based on how many active players and Teletubbies attempted to steal, one of the following occurs as a result:

|||||
|-|-:|-:|-:|
||No teletubbies stole|Some teletubbies stole|All teletubbies stole|
|No active non-teletubbies stole|Nothing extra happens|The teletubbies who stole don’t lose their friendship tokens after all|Each teletubby gets their emoji + the 🌈 emoji for the duration of the following turn|
|Some active non-teletubbies stole|Every teletubby loses a Friendship token|Nothing extra happens|The teletubbies don’t lose their Friendship Tokens after all|
|All active non-teletubbies stole|Everyone who stole gets the 👀 emoji for the duration of the following turn|The teletubbies who stole lose an additional Friendship Token|Nothing extra happens|

## 451 &nbsp;~Scary ~Sporty

![451](IMG_6870.jpg)

## 452 &nbsp;~Sporty ~Ginger

Orange pixels on the canvas cannot have their color directly changed to green, blue, black, or white.

Green pixels on the canvas cannot have their color directly changed to purple, red, black, or white.

Purple pixels on the canvas cannot have their color directly changed to orange, yellow, black, or white.
